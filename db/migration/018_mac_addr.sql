BEGIN;
ALTER TABLE subscribers ADD COLUMN mac_addr VARCHAR;
UPDATE meta SET value='18' WHERE key='db_revision';
COMMIT;