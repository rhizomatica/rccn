BEGIN;
ALTER TABLE providers ADD COLUMN codecs VARCHAR;
UPDATE meta SET value='16' WHERE key='db_revision';
COMMIT;