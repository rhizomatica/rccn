BEGIN;

DROP TABLE hlr;

UPDATE meta SET value='15' WHERE key='db_revision';
DELETE FROM meta WHERE key='hlr_sync';

COMMIT;
