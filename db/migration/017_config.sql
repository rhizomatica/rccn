BEGIN;

ALTER TABLE configuration RENAME smsc_shortcode TO charge_outbound_rate_type;
ALTER TABLE configuration ALTER COLUMN charge_outbound_rate_type DROP DEFAULT;
ALTER TABLE configuration ALTER COLUMN charge_outbound_rate_type DROP NOT NULL;

UPDATE meta SET value='17' WHERE key='db_revision';

COMMIT;