#!/usr/bin/env python
############################################################################
#
# RCCN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RCCN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

from config import *
import smtplib
import random
from optparse import OptionParser

def advise(msg):
    from email.mime.text import MIMEText
    text = """
Favor de intervenir y arreglar esta situacion manualmente.
    """
    mail = MIMEText(text + msg )
    mail['Subject'] = msg[:90]
    mail['From'] = 'postmaster@rhizomatica.org'
    mail['To'] = 'postmaster@rhizomatica.org'
    s = smtplib.SMTP('mail')
    s.sendmail('postmaster@rhizomatica.org', advice_email, mail.as_string())
    s.quit()

def check(auth, recent, hours=2, single=''):
    """ Get sub from the local PG db and see their status in riak """
    sub = subscriber.Subscriber()
    cur = db_conn.cursor()
    if single:
        cur.execute("SELECT msisdn,name,authorized FROM Subscribers WHERE msisdn=%s", [single])
    if recent == 0:
        cur.execute("SELECT msisdn,name,authorized FROM Subscribers WHERE authorized=%s", str(auth))
    if recent == 1:
        sql = "SELECT msisdn,name,authorized FROM Subscribers WHERE authorized=%s AND created > NOW() -interval '%s hours'" % (str(auth), hours)
        cur.execute(sql)
    if cur.rowcount > 0:
        print 'Subscriber Count: %s ' % (cur.rowcount)
        _subs=cur.fetchall()
        cur.close()
        n=cur.rowcount
        for msisdn,name,authorized in _subs:
            print '----------------------------------------------------'
            print "%s: Checking %s %s" % (n, msisdn, name)
            try:
                imsi=sub.get_imsi_from_msisdn(msisdn)
                print "Local IMSI: \033[96m%s\033[0m" % (imsi)
            except NoDataException:
                msg="""
                Local Subscriber %s from PG Not Found on OSMO HLR!
                """ % msisdn
                advise(msg)
                print "\033[91;1mLocal Subscriber from PG Not Found on OSMO HLR!\033[0m"

            n=n-1
        print '----------------------------------------------------\n'
        

def imsi_clash(imsi, ext1, ext2):
    msg = """
    !! IMSI Clash between %s and %s for %s !! 

    Un IMSI no deberia de estar registrado y autorizado al mismo tiempo
    en mas que una comunidad.

    """ % (ext1,ext2,imsi)
    advise(msg)
    print "\033[91;1m" + msg + "\033[0m" 

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-s", "--single", dest="single",
        help="Push a single number")
    parser.add_option("-c", "--cron", dest="cron", action="store_true",
        help="Running from cron, add a delay to not all hit riak at same time")
    parser.add_option("-r", "--recent", dest="recent",
        help="How many hours back to check for created Subscribers")
    parser.add_option("-n", "--noauth", dest="noauth", action="store_true",
        help="Push Not Authorized Subs to D_HLR")

    (options, args) = parser.parse_args()
    
    if options.noauth:
        auth=0
    else:
        auth=1
    if options.cron:
        wait=random.randint(0,15)
        print "Waiting %s seconds..." % wait
        time.sleep(wait)    
    if options.single:
        check(auth,-1,0,options.single)
        exit()
    if options.recent:
        check(auth,1,options.recent)
    else:
        check(auth,0)
