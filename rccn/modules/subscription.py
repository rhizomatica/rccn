############################################################################
# 
# Copyright (C) 2014 tele <tele@rhizomatica.org>
#
# Subscription module
# This file is part of RCCN
#
# RCCN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RCCN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

# Python3/2 compatibility
# TODO: Remove once python2 support no longer needed.
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.append("..")
from config import *

from modules.subscriber import Subscriber, SubscriberException, NOT_AUTH, AUTH, AUTH_DESC
from modules.sms import SMS, SMSException

NOT_PAID = 0
PAID = 1
INACTIVE = 2
SUB_DESC = {
    NOT_PAID: 'Unpaid',
    PAID:     'Paid',
    INACTIVE: 'Inactive'
}

class SubscriptionException(Exception):
    pass

class Subscription:

    def __init__(self, logger):
        self.logger = logger

    def subscription_info(self):
        sub = Subscriber()
        subs = {}
        for i in range(3):
            subs[i] = self.get_subscriptions_by_payment_status(i)
        print('---'*40+'\n')
        for s in subs:
            for number in subs[s]:
                print('PostGres: \033[1;3'+str(s+1)+'m'+number[0]+':\033[0m '+SUB_DESC[s])
                info = sub.print_vty_hlr_info(number[0])
                if "No subscriber for msisdn" in info:
                    print('OsmoHLR: '+info)
                    print("Checking for 5 digit extension")
                    info = sub.print_vty_hlr_info(number[0][-5:])
                print('OsmoHLR: '+ info)
                print('---'*40+'\n')

    def get_subscriptions_by_payment_status(self, status):
        # get all subscribers that haven't paid yet
        # Shouldn't we only do this for those who are actually authorised?
        try:
            cur = db_conn.cursor()
            cur.execute('SELECT msisdn FROM subscribers WHERE subscription_status = %(status)s', {'status':status})
            count = cur.rowcount
            if count > 0:
                subscribers_list = cur.fetchall()
                self.logger.info('PG_SUB: Found %s subscriber(s) with %s subscription.',
                                 count, SUB_DESC[status])
                db_conn.commit()
                return subscribers_list
            else:
                db_conn.commit()
                self.logger.info('PG_SUB: No %s subscribers found.', SUB_DESC[status])
                return []
        except psycopg2.DatabaseError as e:
            raise SubscriptionException('PSQL error getting subscribers subscription_status: %s' % e)

    def set_subscription_status(self, msisdn, status):
        try:
            cur = db_conn.cursor()
            cur.execute('UPDATE subscribers SET subscription_status=%(status)s WHERE msisdn=%(msisdn)s LIMIT 1',
                        {'status': status, 'msisdn': msisdn})
            count = cur.rowcount
            if count > 0:
                db_conn.commit()
                return count
            else:
                self.logger.info('PSQL No subscribers to update status found')
        except psycopg2.DatabaseError as e:
            raise SubscriptionException('PSQL error in updating subscriptions status: %s' % e)

    def update_subscriptions(self, status):
        try:
            cur = db_conn.cursor()
            cur.execute('UPDATE subscribers SET subscription_status=%(status)s', {'status': status})
            count = cur.rowcount
            if count > 0:
                db_conn.commit()
                return count
            else:
                self.logger.info('PSQL No subscribers to update status found')
        except psycopg2.DatabaseError as e:
            raise SubscriptionException('PSQL error in updating subscriptions status: %s' % e)

    def deactivate_subscriptions(self, msg):
        ''' This would generally be run in TIC networks on the 7th day of the month.
            We will find all subs who are still auth'd but have not been marked as paid,
            mark them as unauthorised and change the status to Inactive. '''
        try:
            sms = SMS()
            sub = Subscriber()
            cur = db_conn.cursor()
            cur.execute('SELECT msisdn FROM subscribers WHERE subscription_status = 0 AND authorized = 1')
            count = cur.rowcount
            if count > 0:
                self.logger.info('Found %d subscribers to be deactivated' % count)
                subscribers_list = cur.fetchall()
                db_conn.commit()
                for mysub in subscribers_list:
                    self.logger.debug('Send SMS that account is deactivated to %s' % mysub[0])
                    sms.send(config['smsc'], mysub[0], msg)
                    try:
                        sub.authorized(mysub[0], INACTIVE)
                        sub.subscription(mysub[0], INACTIVE)
                    except SubscriberException as e:
                        raise SubscriptionException('PSQL error in deactivating subscription: %s' % e)
            else:
                db_conn.commit()
                self.logger.info('No subscribers need to be deactivated.')
        except psycopg2.DatabaseError as e:
            raise SubscriptionException('PSQL error in checking subscriptions to deactivate: %s' % e)


    def send_subscription_fee_notice(self, msg):
        # get all subscribers
        try:
            sub = Subscriber()
            subscribers_list = sub.get_all()
        except SubscriberException as e:
            raise SubscriptionException('%s' % e)

        sms = SMS()

        for mysub in subscribers_list:
            self.logger.debug("Send sms to %s %s" % (mysub[1], msg))
            sms.send(config['smsc'], mysub[1], msg)

    def send_subscription_fee_reminder(self, msg):
        try:
            subscribers_list = self.get_subscriptions_by_payment_status(NOT_PAID)
        except SubscriptionException as e:
            raise SubscribtionException('ERROR in getting unpaid subscriptions')

        sms = SMS()
        sub = Subscriber()

        for mysub in subscribers_list:
            package = sub.get_package(mysub[0])
            self.logger.debug("Send sms to %s %s" % (mysub[0], msg))
            sms.send(config['smsc'], mysub[0], msg)
            if package > 0:
                self.logger.info("Deactivate Package for %s", mysub[0])
                sub.reset_package(mysub[0])
                sms.send(config['smsc'], mysub[0], "Su paquete ha sido desactivado.")
