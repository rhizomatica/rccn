############################################################################
# Copyright (C) 2022 keith <keith@rhizomatica.org>
#
# Distributed GSM module
# This file is part of RCCN
#
# RCCN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RCCN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

import os
import json
from subprocess import check_output

class dGSM:

    def __init__(self):
        self.bin = '/usr/bin/osmo-mslookup-client'

    def get_location_by_msisdn(self, number, timeout=1000):
        if not os.path.isfile(self.bin):
            return False

        query = ("%s-@sip.voice.%s.msisdn" % (timeout, number))
        res = check_output(
            [self.bin, '-f', 'json', query])
        if not isinstance(res, str):
            return False
        data = json.loads(res)
        try:
            if data['result'] == 'not-found':
                return False
            return str(json.loads(res)['v4'][0])
        except KeyError as ke:
                return False

    def get_home_hlr_by_imsi(self, imsi, timeout=1000, all=False):
        if not os.path.isfile(self.bin):
            return False

        query = ("%s-@gsup.hlr.%s.imsi" % (timeout, imsi))
        if all:
            results = []
            res = check_output(
                [self.bin, '-f', 'json', '-A', query])
        else:
            res = check_output(
                [self.bin, '-f', 'json', query])
        if not isinstance(res, str):
            return False
        for line in res.splitlines():
            data = json.loads(line)
            try:
                if data['last'] == 'true' and data['result'] == 'not-found':
                    return False
                ip = str(json.loads(line)['v4'][0])
                if not all:
                    return ip
                if not ip in results:
                    results.append(ip)
            except KeyError as ke:
                return False
        return results
