#!/usr/bin/env python
############################################################################
#
# Copyright (C) 2014 tele <tele@rhizomatica.org>
#
# This file is part of RCCN
#
# RCCN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RCCN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
"""
Rhizomatica HLR Sync.
"""

import dateutil.parser as dateparser
from config import *
import random
from optparse import OptionParser
import code

def update_sync_time(time):
    try:
        cur = db_conn.cursor()
        cur.execute("UPDATE meta SET value=%(time)s WHERE key='hlr_sync'", {'time': str(time)})
        db_conn.commit()
    except psycopg2.DatabaseError as e:
        hlrsync_log.error('PG_HLR unable to update hlr sync timestmap db error: %s' % e)

def get_last_sync_time():
    try:
        cur = db_conn.cursor()
        cur.execute("SELECT value FROM meta WHERE key='hlr_sync'")
        sync_time = cur.fetchone()
        if sync_time != None:
            return int(sync_time[0])
        else:
            hlrsync_log.error('Unable to get last sync time. exit')
            sys.exit(1)
    except psycopg2.DatabaseError as e:
        hlrsync_log.error('PG_HLR database error getting last sync time')
        sys.exit(1)

def cleanup_hlrs():
    prefix = config['internal_prefix']
    try:
        sq_hlr = sqlite3.connect(sq_hlr_path)
        sq_hlr_cursor = sq_hlr.cursor()
        sq_hlr_cursor.execute("SELECT id, imsi, msisdn FROM subscriber "
                              "WHERE msisdn NOT LIKE ? AND length(msisdn) = 11 AND nam_cs = 1",
                              [(prefix+'%')])
        osmo_hlr_subs = sq_hlr_cursor.fetchall()

    except sqlite3.Error as ex:
        sq_hlr.close()
        print ('Sqlite error: %s' % ex.args[0])
        return

    for osmo_sub in osmo_hlr_subs:
        hlrsync_log.info("Authorised HLR Entry: id [%s] IMSI[%s] msisdn[%s] not local",
                         osmo_sub[0], osmo_sub[2], osmo_sub[1])
        try:
            sq_hlr_cursor.execute("DELETE FROM subscriber WHERE id = ? LIMIT 1", [osmo_sub[0]])
            sq_hlr.commit()
        except sqlite3.Error as ex:
            sq_hlr.close()
            print ('Sqlite error: %s' % ex.args[0])
            return

    sq_hlr.close()

def hlr_sync(hours,until):
    return

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-c", "--cron", dest="cron", action="store_true",
        help="Running from cron, add a delay to not all hit riak at same time")
    parser.add_option("-s", "--since", dest="hours",
        help="Sync from the d_hlr since HOURS ago instead of last update")
    parser.add_option("-u", "--until", dest="until",
        help="Sync from the d_hlr until HOURS ago instead of now, requires -s")
    parser.add_option("-m", "--minutes", dest="minutes",
        help="Sync from the d_hlr since MINUTES ago (-s) instead of last update")
    parser.add_option("-e", "--expunge", dest="expunge", action="store_true",
        help="Clean removed subscribers from the HLRs")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
        help="Turn on debug logging")
    (options, args) = parser.parse_args()
    
    if options.debug:
        hlrsync_log.setLevel(logging.DEBUG)
    else:
        hlrsync_log.setLevel(logging.INFO)

    if options.cron:
        wait=random.randint(0,120)
        print "Waiting %s seconds..." % wait
        time.sleep(wait)

    if options.expunge:
        cleanup_hlrs()
        sys.exit()

    if options.hours or options.minutes:
        if options.until:
            until=int(options.until)
        else:
            until=0
        if options.minutes:
            hours=float(options.minutes+'.00')/60
        else:
            hours=int(options.hours)
        hlr_sync(hours,until)
    else:
        hlr_sync(0,0)
