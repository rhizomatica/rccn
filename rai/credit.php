<?php 

require_once('modules/subscriber.php');
require_once('modules/credit.php');
require_once('modules/configuration.php');
require_once('include/menu.php');
require_once('include/header.php');

print_menu('credits'); ?>

<script type="text/javascript">
	$(function() {

	$(".subscriber_no").autocomplete(
        {
          source: "/rai/ajax.php?service=numbers",
          minLength: 2,
          search: function(event, ui) { $('#subscriber_no').addClass('ac_loading'); },
          response: function(event, ui) {
			$('#subscriber_no').removeClass('ac_loading');
        }
          });
	});
</script>
<br/><br/><br/><br/>

<?php

function foot() {
	print "</div></body></html>";
	exit();
}

function transfer_credit() {
	echo "<center>";
	$cred = new Credit();
	try {
		$cred->transfer(
			$_SESSION['tx_credit']['tx_src'],
			$_SESSION['tx_credit']['tx_dest'],
			$_SESSION['tx_credit']['amount']);
		echo "<img src='img/true.png' width='200' height='170' /><br/><br/>";
		echo "<span style='font-size: 20px;'>"._("Credit of")." <b>".$_SESSION['tx_credit']['amount']."</b> ";
		echo _("pesos successfully transfered to subscriber")." <b>".$_SESSION['tx_credit']['dest_user']."</b>.</span><br/><br/><br/>";
		echo "<a href='credit.php?action=transfer'><button class='b1'>"._("Go Back")."</button></a>";
		$_SESSION['tx_credit'] = array();
	} catch (CreditException $e) {
		echo "<img src='img/false.png' width='200' height='170' /><br/><br/>";
		echo "<span style='font-size: 20px; color: red;'>"._("ERROR TRANFERING BALANCE!")."<br/>".$e->getMessage()." </span><br/><br/><br/><br/>";
		echo "<a href='credit.php?action=transfer'><button class='b1'>"._("Go Back")."</button></a>";
	}
	echo "</center>";
}

function add_credit() {
	echo "<center>";
	$cred = new Credit();
	try {
		$cred->add($_SESSION['add_credit']['subscriber_no'],$_SESSION['add_credit']['amount']);
		echo "<img src='img/true.png' width='200' height='170' /><br/><br/>";
		echo "<span style='font-size: 20px;'>"._("Credit of")." <b>".$_SESSION['add_credit']['amount']."</b> ";
		echo _("pesos successfully added to subscriber")." <b>".$_SESSION['add_credit']['user']."</b>.</span><br/><br/><br/>";
		echo "<a href='credit.php'><button class='b1'>"._("Go Back")."</button></a>";
		$_SESSION['add_credit'] = array();
	} catch (CreditException $e) {
		echo "<img src='img/false.png' width='200' height='170' /><br/><br/>";
		echo "<span style='font-size: 20px; color: red;'>"._("ERROR UPDATING BALANCE!")."<br/>".$e->getMessage()." </span><br/><br/><br/><br/>";
		echo "<a href='credit.php'><button class='b1'>"._("Go Back")."</button></a>";
	}
	echo "</center>";
}

function check_data_tx() {
	$_s = $_SESSION['tx_credit'];
	$error_txt = "";
	if ($_s['tx_src'] != "" && $_s['tx_src'] == $_s['tx_dest']) {
		print_form_transfer(_("Source and Destination are the same."));
		foot();
	}
	if ($_s['tx_src'] == "") {
		$error_txt .= _("Source Subscriber number")." "._("is empty")."<br />";
	}
	if ($_s['tx_dest'] == "") {
		$error_txt .= _("Destination Subscriber number")." "._("is empty")."<br />";
	}
	$amount = $_s['amount'];
	if ($amount == "" || $amount == 0 || !is_numeric($amount)) {
		$error_txt .= _("Invalid amount");
	}
	if ($error_txt != "") {
		print_form_transfer($error_txt);
		foot();
	}
}

function check_data_add() {
	$error_txt = "";
	if ($_SESSION['add_credit']['subscriber_no'] == "") {
		$error_txt .= _("Subscriber number")." "._("is empty")."<br/>";
	}
	$amount = $_SESSION['add_credit']['amount'];
	if ($amount == "" || $amount == 0 || !is_numeric($amount)) {
		$error_txt .= _("Invalid amount")."<br/>";
	}
	if ($error_txt != "") {
		print_form_add($error_txt);
		foot();
	}
}

function drop_down_subscriber_number($internalprefix='', $label, $name="subscriber_no", $sess="add_credit") {
?>
	<label><?=$label?>
	<span class="small"><?= _("Enter digits to search for a subscriber.") ?></span>
	</label>
	<input type="text" placeholder="<?=$internalprefix?>XXXXX" name="<?=$name?>" class="subscriber_no"
		   value="<?=$_SESSION[$sess][$name]?>"/>
<?
}

function print_form_add($errors="") {
	global $internalprefix;
?>
<div id="stylized" class="myform add_credit_form">
	<form id="form" name="form" method="post" action="credit.php">
	<input type="hidden" name="action" value="add">
	<h1><?= _("Add Credit") ?></h1><br/>
	<span style='color: red; font-size: 14px;'><?= $errors ?></span><br/>

	<? drop_down_subscriber_number($internalprefix, _("Subscriber number")); ?>

	<label><?= _("Amount") ?>
	<span class="small"><?= _("Amount to add") ?></span>
	</label>
	<input autocomplete="off" placeholder="000" type="text" name="amount" id="amount"
		   value="<?=$_SESSION['add_credit']['amount']?>"/>

	<br /><br />

	<button type="submit" id="add_credit" name="add_credit" value="1"><?= _("Next") ?> <img src="img/chevron-double-right.svg" /> </button>
	<div class="spacer"></div>
	</form>
</div>
<?
}

function print_form_transfer($errors="") {
	global $internalprefix;
?>
<div id="stylized" class="myform add_credit_form">
	<form id="form" name="form" method="post" action="credit.php">
	<input type="hidden" name="action" value="transfer">
	<h1><?= _("Transfer Credit") ?></h1><br/>
	<? if ($errors!="") { ?>
	<span style='color: red; font-size: 14px;'><?= $errors ?><br /></span><br/>
	<? } ?>

	<? drop_down_subscriber_number($internalprefix, _("Source Number"), "tx_src", "tx_credit"); ?>
	<? drop_down_subscriber_number($internalprefix, _("Destination Number"), "tx_dest", "tx_credit"); ?>

	<label><?= _("Amount") ?>
	<span class="small"><?= _("Amount to transfer") ?></span>
	</label>
	<input autocomplete="off" placeholder="000" type="text" name="amount" id="amount"
		   value="<?=$_SESSION['tx_credit']['amount']?>"/><br/>
	<button type="submit" id="tx_credit" name="tx_credit" value="1"><?= _("Next") ?> <img src="img/chevron-double-right.svg" /> </button>
	<div class="spacer"></div>
	</form>
</div>
<?
}

function print_confirm() { ?>
	<div id="stylized" class="confirm">
	<h1><?= _("Add Credit") ?></h1><br/>
	<?= _('Are you sure you want to add credit of') ?>
	<span class="notice"><?=$_SESSION['add_credit']['amount']?></span>
	<?= _('pesos to the account of')?><br />
	<span class="notice_r"><?=$_SESSION['add_credit']['user']?></span><?=_('?')?><br />
	<?= _('This will result in a new balance of')?>
	<span class="notice"><?=($_SESSION['add_credit']['amount'] + $_SESSION['add_credit']['prev_amount'])?></span> pesos.
	<form id="form" name="form" method="post" action="credit.php">
	<button type="submit" name="add_credit" value="-1"><img src="img/x-circle-fill.svg" /> <?= _("Cancel") ?></button>
	<button type="submit" name="add_credit" value="3"><img src="img/chevron-double-left.svg" /> <?= _("Modify") ?></button>
	<button type="submit" name="add_credit" value="2"><?= _("Add") ?> <img src="img/check-circle-fill.svg" /></button>
	<input type="hidden" name="action" value="add">
	</form>
	</div>
<? }

function print_confirm_tx() {
	$_s = $_SESSION['tx_credit']; ?>
	<div id="stylized" class="confirm">
	<h1><?= _("Transfer Credit") ?></h1><br/>
	<? if ($_s['src_balance_pre'] < $_s['amount']) {
		?> <span class="notice_r"> <?
		print _("Source Balance is too low: ")."$".$_s['src_balance_pre'];
		?> </span> <?
		$disabled=true;
	} else { ?>
		<?= _('Are you sure you want to transfer credit of') ?>
		<span class="notice"><?=$_s['amount']?></span>
		<?= _('pesos to the account of')?><br />
		<span class="notice_r"><?=$_s['dest_user']?></span><?=_('?')?><br />
		<?= _('This will result in a new balance of')?>
		<span class="notice"><?=($_s['amount'] + $_s['dest_balance_pre'])?></span> pesos.<br /><br />
		<?= _('The resulting balance for the source account of ').
			'<br /><span class="notice_r">'.$_s['src_user'].'</span>'.
			_(' will be ')?>
		<span class="notice"><?=($_s['src_balance_pre'] - $_s['amount'])?></span> pesos.
	<? } ?>
	<form id="form" name="form" method="post" action="credit.php">
	<button type="submit" name="tx_credit" value="-1"><img src="img/x-circle-fill.svg" /> <?= _("Cancel") ?></button>
	<button type="submit" name="tx_credit" value="3"><img src="img/chevron-double-left.svg" /> <?= _("Modify") ?></button>
	<button type="submit" <?=($disabled==true) ? "disabled class='button_disabled'" : ""?> name="tx_credit" value="2"><?= _("Transfer") ?> <img src="img/check-circle-fill.svg" /></button>
	<input type="hidden" name="action" value="transfer">
	</form>
	</div>
<? }

# main.
if (isset($_GET['action'])) {
	$_POST['action'] = $_GET['action'];
}

if (!isset($_POST['action'])) {
	print_form_add();
	foot();
}

$site = new Configuration();
$info = $site->getSite();
$internalprefix = $info->postcode.$info->pbxcode;

switch ($_POST['action']) {
	case 'add':
		$sess = "add_credit";
		if (!$_SESSION[$sess]) {
			$_SESSION[$sess] = array('subscriber_no' => '', 'amount' => NULL);
		}
		//var_dump($_SESSION);
		if (!isset($_POST['add_credit'])) {
			print_form_add();
			foot();
		}
		if ($_POST['add_credit'] == "1") { // Next
			$_SESSION[$sess]['subscriber_no'] =
				(isset($_POST['subscriber_no'])) ? $_POST['subscriber_no'] : '';
			$_SESSION[$sess]['amount'] =
				(isset($_POST['amount'])) ? $_POST['amount'] : '0';
			check_data_add();
			$sub = new Subscriber();
			try {
				$sub->get($_POST['subscriber_no']);
			} catch (SubscriberException $e) {
				print_form_add($e->getMessage());
				foot();
			}
			$_SESSION[$sess]['user'] = $sub->name;
			$_SESSION[$sess]['prev_amount'] = $sub->balance;
			print_confirm();
			foot();
		}
		if ($_POST['add_credit'] == "-1") { // Cancel
			$_SESSION[$sess] = array();
			print_form_add();
			foot();
		}
		if ($_POST['add_credit'] == "3") { // Modify
			print_form_add();
			foot();
		}
		if ($_POST['add_credit'] == "2") { // Add
			check_data_add();
			add_credit();
			foot();
		}
		break;

	case 'transfer':
		$sess = "tx_credit";
		if (!$_SESSION[$sess]) {
			$_SESSION[$sess] = array('src' => '',
						 'dest' => '',
						 'amount' => NULL
						);
		}
		if (!isset($_POST['tx_credit'])) {
			print_form_transfer();
			foot();
		}
		if ($_POST['tx_credit'] == "1") { // Next
			$_SESSION[$sess]['tx_src'] =
				(isset($_POST['tx_src'])) ? $_POST['tx_src'] : '';
			$_SESSION[$sess]['tx_dest'] =
				(isset($_POST['tx_dest'])) ? $_POST['tx_dest'] : '';
			$_SESSION[$sess]['amount'] =
				(isset($_POST['amount'])) ? $_POST['amount'] : '0';
			check_data_tx();
			$sub = new Subscriber();
			try {
				$sub->get($_SESSION[$sess]['tx_src']);
			} catch (SubscriberException $e) {
				print_form_transfer($e->getMessage());
				foot();
			}
			$_SESSION[$sess]['src_user'] = $sub->name;
			$_SESSION[$sess]['src_balance_pre'] = $sub->balance;
			try {
				$sub->get($_SESSION[$sess]['tx_dest']);
			} catch (SubscriberException $e) {
				print_form_transfer($e->getMessage());
				foot();
			}
			$_SESSION[$sess]['dest_user'] = $sub->name;
			$_SESSION[$sess]['dest_balance_pre'] = $sub->balance;
			print_confirm_tx();
			foot();
		}
		if ($_POST['tx_credit'] == "-1") { // Cancel
			$_SESSION[$sess] = array();
			print_form_transfer();
			foot();
		}
		if ($_POST['tx_credit'] == "3") { // Modify
			print_form_transfer();
			foot();
		}
		if ($_POST['tx_credit'] == "2") { // Add
			check_data_tx();
			transfer_credit();
			foot();
		}
		break;
	default:
		$sess="unknown"; // Exit Anyway.
		foot();
		break;
}
?>
		</div>
	</body>

</html>
