<?php 
require_once('include/header.php');
require_once('include/menu.php');
require_once('modules/subscriber.php');
require_once('modules/configuration.php');
require_once('modules/credit.php');

$conf   = new Configuration();
$info   = $conf->getSite();
$con    = $conf->getConfig();

$internalprefix = $info->postcode.$info->pbxcode;

?>
	<? print_menu('subscribers'); ?>

    <script type="text/javascript" charset="utf-8">

        var sip_calling_url='<?= $con->sip_calling_url; ?>';

        function call_number(data) {
            number = $(data).data('msisdn');
            if (number == "" || sip_calling_url == "") { return }
            data.target="_blank"
            window.open(sip_calling_url+number)
        }

        $(document).ready(function() {

            var oTable = $('#subscribers').dataTable( {
                <?
                $lang_file = 'js/'.$_SESSION['lang'].'.txt';
                if (file_exists($lang_file)) {
                    echo '"oLanguage": { "sUrl": "'.$lang_file.'" },';
                }
                ?>
                "aLengthMenu": [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "<?=_("All")?>" ] ],
                "sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "aaSorting": [[ 0, "desc" ]],
                "aoColumnDefs": [
                    {
                        "aTargets": [2],
                        "sClass": "status center",
                        "mRender": function(data, type, full) {
                            pack = (data[1] == 1) ? "<span class='package-ind'> P</span>" : "";
                            return data[2]+pack;
                        },
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            switch (parseInt(oData[2][0])) {
                                case 0:
                                    $(nTd).addClass('unpaid');
                                    break;
                                case 1:
                                    $(nTd).addClass('paid');
                                    break;
                                case 2:
                                    $(nTd).addClass('suspend');
                                    break;
                            }
                        }
                    },
                    {
                        "aTargets": [3],
                        "sClass": "auth",
                        "mData": null,
                        "sDefault": "",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (oData[3] === true) {
                                $(nTd).addClass('authorized')
                            }
                            if (oData[3] === false) {
                                $(nTd).addClass('unauthorized')
                            }
                        }
                    },
                    {
                        "aTargets": [4],
                        "sClass": "msisdn",
                        "mRender": function(data, type, full) {
                            icon = (data[3]) ? " ☎️" : "";
                            return "<span>"+data[0]+"</span>"+icon;
                        },
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (sip_calling_url == "") { return }
                            if (oData[4][2] ) {
                                $(nTd).addClass('roaming');
                                return;
                            }
                            if (oData[4][1] || oData[4][3]) {
                                $(nTd).addClass('attached')
                            } else {
                                $(nTd).addClass('detached')
                            }
                        }
                   },
                   {
                        "aTargets": [8],
                        "mData": null,
                        "mRender": function (data, type, full) {
                            sub = full[4][0].match(/\d{6}\d+/);
                            try {
                                if (sub[0].substr(0,6) != <?=$internalprefix?>) return '';
                                return '<a href="subscriber_edit.php?id='+sub[0]+'" class="pop"><img src="img/edit.png" alt="Edit" valign="middle" /></a> | <a href="subscriber_delete.php?id='+sub+'" class="pop"><img src="img/delete.png" alt="Delete" valign="middle" /></a> | <span data-msisdn='+sub+' class="sip_calling">✆<span>';
                            } catch(TypeError) {
                                return '';
                            }
                        },
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (oData[4][1] || oData[4][3]) {
                                $(nTd).addClass('connected')
                            }
                        },
                        "sClass": "dt_actions"
                    },
                    { "bSearchable": false, "aTargets": [0] },
                    { "bSearchable": false, "aTargets": [1] },
                    { "bSearchable": false, "aTargets": [2] },
                    { "bSearchable": false, "aTargets": [6] }
                ],
                "aoColumns": [
                        {},{},{},{},{},{"sClass": "name"},{},{}
                ],
                "fnDrawCallback": function () {
                        $(".pop").fancybox({
                            'width'             : '50%',
                            'height'            : '90%',
                            'autoScale'         : false,
                            'type'              : 'iframe',
                            'onClosed'          : function() {
                                  parent.location.reload(true);
                            }
                        });
                        if (sip_calling_url == "") {
                            $('span.sip_calling').hide()
                        } else {
                            $('.connected span.sip_calling').click(function() {
                                call_number(this)
                            })
                        }
                },
                "sAjaxSource": "subscribers_processing.php",
                "bStateSave": true,

                "fnInitComplete": function(oSettings, json) {
                    if (oSettings.oLoadedState != null) {
                      sVal=oSettings.oLoadedState.oSearch.sSearch
                    } else {
                      sVal = ''
                    }
                    consel=''
                    dissel=''
                    rsel=''
                    if (sVal == 'RAI-all-connected') { consel="selected" }
                    if (sVal == 'RAI-all-disconnected') { dissel="selected" }
                    if (sVal == 'RAI-all-roaming') { rsel="selected" }
                    sel='<div style="display:table"><select id="conSelect" style="width:50px">' +
                    '<option value="">&nbsp;</option>' +
                    '<option '+consel+' value="RAI-all-connected" data-image="img/led-green.gif"></option>' +
                    '<option '+dissel+' value="RAI-all-disconnected" data-image="img/led-red.gif"></option>' +
                    '<option '+rsel+' value="RAI-all-roaming" data-image="img/led-roam.gif"></option>' +
                    '</select></div>'
                    $('#subscribers thead tr th:nth-child(5)').prepend(sel)
                    $('#conSelect').msDropDown()
                    $('#conSelect').change(function() {
                        $("#dynamic [name='subscribers_length']").val('-1')
                        $('#subscribers_filter input').val('')
                        if (this.value=='') {
                                oTable.fnSettings()._iDisplayLength = 10;
                                $("#dynamic [name='subscribers_length']").val('10')
                        } else {
                                oTable.fnSettings()._iDisplayLength = -1;
                        }
                        oTable.fnFilter (this.value, null,false,false,false)
                      })
                    if (oSettings.oLoadedState != null) {
                        sVal=oSettings.oLoadedState.aoSearchCols[3].sSearch
                    } else {
                        sVal=''
                    }
                    authsel=''
                    noauthsel=''
                    if (sVal == '1') { authsel="selected" }
                    if (sVal == '0') { noauthsel="selected" }
                    sel='<div style="display:table"><select id="authSelect" style="width:50px">' +
                    '<option value="">&nbsp;</option>' +
                    '<option '+authsel+' value="1" data-image="img/unlock.gif"></option>' +
                    '<option '+noauthsel+' value="0" data-image="img/lock.gif"></option> </select></div>'
                    $('#subscribers thead tr th:nth-child(4)').prepend(sel)
                    $('#authSelect').msDropDown()
                    $('#authSelect').change(function() {
                        //$("#dynamic [name='subscribers_length']").val('-1')
                        //$('#subscribers_filter input').val('')
                        oTable.fnFilter (this.value, 3,false,false,false)
                      })
                }
            } );
        });
                        
	</script><br/>
    <?php
        try {
            $sub = new Subscriber();
            $unpaid_subscribers = $sub->get('unpaid_subscription');
            $unauthorized_subscribers = $sub->get('unauthorized');
            $paid_subscribers = $sub->get('paid_subscription');
            $online = $sub->get('online');
            $offline = $sub->get('offline');
            $roaming = $sub->get('all_roaming');
            $cred = new Credit();
            $credit = json_decode($cred->get_month_credit(date('Y'),date('m')));
            $credit_sold=$credit[0][0];
            $credit_used=$credit[1][0];

            echo "<div>";
            echo "<div class='left_box' style='margin-left:10px;'>"._("Unpaid subscription").": <b>$unpaid_subscribers</b></div>";
            echo "<div class='left_box'>"._("Unauthorized").": <b>$unauthorized_subscribers</b></div>";
            echo "<div class='left_box'>"._("Paid subscription").": <b>$paid_subscribers</b></div>";
            echo "<div class='left_box'>"._("Credit Sold").": <b>$$credit_sold</b></div>";
            echo "<div class='left_box'>"._("Credit Used").": <b>$$credit_used</b></div>";                                
            echo "<div class='left_box'>"._("Online").": <b>$online</b> ("._("Roaming").": <b>$roaming</b>)</div>";
            echo "<div class='left_box'>"._("Offline").": <b>$offline</b></div>";
            echo "</div>";

        }
        catch (SubscriberException $e) { }
    ?>


			<h1><?= _('Subscribers Phones') ?></h1><br/>
			<div id="dynamic">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="subscribers">
	<thead>
		<tr>
			<th align='left' width='12%'><?= _("Activation date") ?></th>
                        <th align='left' width='14%'><?= _('Subscription date') ?></th>
                        <th width='12%'><?= _("Subscription Status") ?></th>
			<th width="7%"> <?= _("Authorized") ?></th>
			<th align='left' width='12%'><?= _("Number") ?></th>
			<th align='left'><?= _("Name") ?></th>
			<th align='left'><?= _("Balance") ?></th>
			<th align='left'><?= _("Location") ?></th>
			<th align='left'><?= _("Actions") ?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="5" class="dataTables_empty"><?= _("Loading data from server") ?></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
                        <th align='left' width='12%'><?= _("Activation date") ?></th>
                        <th align='left' width='14%'><?= _('Subscription date') ?></th>
                        <th width='12%'><?= _("Subscription Status") ?></th>
                        <th width="7%"><?= _("Authorized") ?></th>
                        <th align='left' width='12%'><?= _("Number") ?></th>
                        <th align='left'><?= _("Name") ?></th>
                        <th align='left'><?= _("Balance") ?></th>
			<th align='left'><?= _("Location") ?></th>
                        <th align='left'><?= _("Actions") ?></th>
		</tr>
	</tfoot>
</table>
			</div>
			<div class="spacer"></div>
				
		</div>
	</body>

</html>
