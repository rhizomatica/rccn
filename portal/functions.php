<?
require_once("../rai/modules/httpful.phar");

function get_send_code($phone) {
	$rapi = "http://localhost:8085/";
	# Check if the phone number exists.
	try {
		$re = \Httpful\Request::get($rapi."configuration/site")->expectsJson()->send();
		$config = $re->body;
		$site_prefix = $config->postcode.$config->pbxcode;
		$_SESSION['fullphone']=$site_prefix.$phone;
		$re = \Httpful\Request::get($rapi."subscriber/".$_SESSION['fullphone'])->expectsJson()->send();
	} catch (Httpful\Exception\ConnectionErrorException $e) {
		$_SESSION['fullphone']='';
		return -1;
	}

	if (!$re->body) return -1;
	if (is_object($re->body) && $re->body->status=="failed") {
		$_SESSION['fullphone']='';
		$_SESSION['error']=$re->body->error;
		return -2;
	}
	# get a code
	$_SESSION['code'] = rand(12345,99999);
	$text = $_SESSION['code']." es el código para dar aceso a este dispositivo en Wifi.";
	$data = array("source" => '10000',
		      "destination" => $_SESSION['fullphone'],
		      "text" => $text
		);
	try {
		$re = \Httpful\Request::post($rapi."sms/send")->body($data)->sendsJson()->send();
		if ($re->code != 201) {
			return -1;
		}
	} catch (Httpful\Exception\ConnectionErrorException $e) {
		return -1;
	}
}

function GetClientMAC($ipAddress='') {
	/* Based on code from GPL 'OpenVoucher' project */
	if($ipAddress=='')
	{
		$ipAddress=$_SERVER['REMOTE_ADDR'];
	}
	$macAddr='';

	$arp=shell_exec('/usr/sbin/arp'.' -a '.$ipAddress);

	$x=strpos($arp,':');
	$begin=$x-2;
	$macAddr=substr($arp,$begin,17);

	return $macAddr;
}

function verify_code($code) {
	if (!is_numeric($_SESSION['code']) || $_SESSION['code'] != $code) return -2;
	$rapi = "http://localhost:8085/";
	$data = array("msisdn" => $_SESSION['fullphone'],
		      "mac_addr" => GetClientMAC()
		);

	try {
		$re = \Httpful\Request::post($rapi."subscriber/portal_auth")->body($data)->sendsJson()->send();
		if ($re->code != 201) {
			return $re->code;
		}
	} catch (Httpful\Exception\ConnectionErrorException $e) {
		return -1;
	}
}

function code_form() { ?>
  <div class="instruct">
	Ingresa el código que se recibió por SMS.
  </div>

  <form autocomplete="off" method="POST" action="/index.php">
	<input autocomplete="false" type="text" inputmode="numeric" pattern="[0-9]*" name="code" maxlength="5" size="5"><br />
	<input type="submit" name="access" value="Listo">
  </form>
  <?
}

function number_form() { ?>
  <div class="instruct">
	Ingresa los últimos 5 digitos de tu número celular de la Telefonía Comunitaria.<br />
	Recibirás un código por SMS.
  </div>

  <form method="POST" action="/index.php">
	<input inputmode="numeric" pattern="[0-9]*" name="phone" maxlength="5" size="5" value="<?=$_SESSION['phone']?>"><br />
	<input type="submit" name="start" value="Listo">
  </form>
  <?
}

?>
