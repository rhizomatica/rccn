#!/bin/bash

#                                                                          #
# Note that currently osmo-msc does internal cleaning of the SMS database. #
# So for a split stack we probably do not want to run this script.         #
#                                                                          #

. /etc/profile.d/rccn-functions.sh

LOGFILE="/var/log/sms_cleanup.log"
SMS_DB=$OSMO_SMS
SMS_DB_BKP="/home/rhizomatica/sms/hlr_`date '+%d%m%Y'`.sqlite3"

function logc() {
	txt=$1
	echo "[`date '+%d-%m-%Y %H:%M:%S'`] $txt" >> $LOGFILE
}

logc "Run database cleanup. Current DB size: `ls -sh $SMS_DB | awk '{print $1}'`"
logc "Make backup copy of SMS db"
#cp -f $SMS_DB $SMS_DB_BKP

total_sms=`echo 'select count(*) from SMS;' | sqlite3 -init <(echo .timeout 1000) $SMS_DB`
total_sms_delivered=`echo 'select count(*) from SMS where sent is not null;' | sqlite3 -init <(echo .timeout 1000) $SMS_DB`
total_sms_old=`echo "select count(*) from SMS where created < datetime('now', '-4 day');" | sqlite3 -init <(echo .timeout 1000) $SMS_DB`

logc "Total SMS: $total_sms Delivered: $total_sms_delivered SMS older than 4 days: $total_sms_old"
logc "Cleanup DB"

# Delete Any Broadcast SMS to a subscriber that is currently not authorised and also has not been seen for two weeks.
echo "ATTACH \"/var/lib/osmocom/hlr.db\" as hlr; \
DELETE from SMS where src_ton=5 and exists (select msisdn,nam_cs,last_lu_seen \
from hlr.subscriber where msisdn = sms.dest_addr and last_lu_seen < datetime('now', '-14 day') and nam_cs = 0);" | sqlite3 -init <(echo .timeout 1000) $SMS_DB

# Delete any SMS in the database that was created more than 6 hours ago and has been sent.
echo "DELETE from SMS where created < datetime(\"now\", \"-6 hours\") and sent is not null;" | sqlite3 -init <(echo .timeout 1000) $SMS_DB
# Delete any broadcast message that is not delivered after 7 days.
echo "DELETE from SMS where created < datetime(\"now\", \"-7 day\") and src_ton=5 and sent is null;" | sqlite3 -init <(echo .timeout 1000) $SMS_DB

logc "DB size after cleanup: `ls -sh $SMS_DB | awk '{print $1}'`"
